/* Dependencies */
var request     = require('request'),
    cheerio     = require('cheerio');

/* Generic vars */
var produtos      = {},
    cont          = 0,
    contMax       = 0,
    keys          = [],
    novo_preco    = 0,
    transporter   = null,
    mailOptions   = null,
    firebaseKey   = 0,
    element       = '';

/* Constant vars */
var ENABLE_EMAIL  = false,
    TIMEOUT       = 4000;

var getElementWithValue = function(value, $) {
    return $("*:not(script):contains('" + value +"')").last();
};

setInterval(function() {
    request('http://www.exchangerates.org.uk/Pounds-to-Euros-currency-conversion-page.html', function(err, resp, body) {

        if (err) { return; }

        $ = cheerio.load(body);

        console.log($('#shd2a>span').text());
    });
}, TIMEOUT);